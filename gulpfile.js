var gulp = require('gulp'),
  browserSync = require("browser-sync").create(),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
  iconfont = require('gulp-iconfont'),
  iconfontCss = require('gulp-iconfont-css'),
	rigger = require('gulp-rigger');

var runTimestamp = Math.round(Date.now()/1000);

var fontName = 'Icons';

gulp.task('iconFont', function(){
  gulp.src(['app/icons-font/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
    }))
    .pipe(iconfont({
      fontName: fontName
    }))
    .pipe(gulp.dest('app/fonts/icons-font/'));
});
	
gulp.task('sass',function() {
	gulp.src('app/sass/style.sass') //Взять файлы
	.pipe(sass({outputStyle:
		'expended'}).on('error',sass.logError)) //Компилируем Sass
	.pipe(autoprefixer())
	.pipe(gulp.dest('app/css/'))
		.pipe(browserSync.stream());
});

gulp.task('html',function(){
	gulp.src('app/build/*.html')
	.pipe(rigger())
	.pipe(gulp.dest('app/'))
		.pipe(browserSync.stream())
});

gulp.task('compress', function() {
	gulp.src('app/js/*.js' , '!app/js/*.min.js')
    .pipe(rename({suffix: ".min"}))
    .pipe(uglify())
    .pipe(gulp.dest('app/js/'))
		.pipe(browserSync.stream());
});

gulp.task('img_min', function() {
  gulp.src('app/img/*.jpeg')
  .pipe(imagemin())
  .pipe(gulp.dest('app/img'))
});

gulp.task('watch',function(){
	gulp.watch('app/sass/**/*.sass',
		function(event, cb) {
			setTimeout(function(){
			gulp.start('sass');
			}, 1000);
		});
  browserSync.init({
    server: "./app/",
    logPrefix: "Frontend"
  });
	gulp.watch('app/part/**/*.html', ['html']);
	gulp.watch('app/build/*.html',['html']);
	gulp.watch('app/js/*.js',['compress']);
});

gulp.task('default',['watch','sass','html', 'iconFont', 'compress']); //Задание по умолчанию


