$(document).ready(function () {
  var header = $("#main-header");
  var sticky = header.find('.header__nav').offset().top;
  $(window).scroll(function () {
    if (window.pageYOffset >= sticky) {
      header.addClass("sticky");
    } else {
      header.removeClass("sticky");
    }
  });


  var mouseInSearchIcon = false;
  $('.search-icon').hover(function () {
    var description = $(this).find('.search-icon__description');
    var closeBtn = $(this).find('.search-icon__close');
    closeBtn.on('click', function () {
      description.fadeOut();
      mouseInSearchIcon = false;
      $(this).removeClass('active');
    });
    $(this).addClass('active');
    description.fadeIn();
    mouseInSearchIcon = true;
  }, function () {
    var description = $(this).find('.search-icon__description');
    $(this).removeClass('active');
    description.fadeOut();
    mouseInSearchIcon = false;
  });

  // $('body').mouseup(function () {
  //   if (!mouseInSearchIcon)
  //     $('.search-icon__description').fadeOut();
  // });
	//ViewPort 480px
	// var viewport = document.createElement("meta");
	// viewport.setAttribute("name", "viewport");
	// if (screen.width < 480) {
	// 	viewport.setAttribute("content", "width=480");
	// } else {
	// 	viewport.setAttribute("content", "width=device-width, initial-scale=1");
	// }
	// document.head.appendChild(viewport);
	
	$('.mainSlider').slick({
		dots: true
	});

	$('.ourPartners__slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
  });

	$('.header__show-menu').on('click', function () {
    $('.header__nav-menu').toggle(200)
  })
	
	$('.weTrust__items').slick({
		arrows: false,
		dots: true,
		slidesToShow: 3,
		slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
	});

	// header popup
  $('.callback_popup-show').on('click', function () {
    $('.popup-callback').fadeIn();
  })
	$('.callback_popup-hide').on('click', function () {
		$('.popup-callback').fadeOut();
  })

	// toggle question
	$('.answer__question-item').on('click', function () {
		if ($(this).hasClass('answer__question-item--active')) {
      $('.answer__question-item').removeClass('answer__question-item--active');
		} else {
      $('.answer__question-item').removeClass('answer__question-item--active');
      $(this).addClass('answer__question-item--active');
		}
  });

  // side bar menu
	var sidebarMenu = $('.catalog__sidebar-menu');
	
	sidebarMenu.on('click', function () {
		var menu = $(this);
		var dropDown = menu.find('.catalog__drop-down')
			if (!menu.hasClass('active')) {
				menu.addClass('active');
				dropDown.show(500);
			} else {
				menu.removeClass('active');
				dropDown.hide(300);
			}
  });

	var intervalItemBanner = setInterval(function () {
		var banner = $('.catalog__filter-item-banner');

		if (banner.hasClass('active')){
			banner.removeClass('active')
		} else {
			banner.addClass('active')
		}
  }, 2000)

  var customInput = $('.order__table-input').append('<button class="order__table-input-btn inc">+</button>').prepend('<button class="order__table-input-btn dec">-</button>').find('input').val('0 шт');

  $('.order__table-input-btn').on('click', function () {
      var wraper = $(this).parent();
      var input = wraper.find('input');
      var value = parseFloat(input.val());

      if ($(this).hasClass('inc')){
        value++;
      } else {
        value > 0 ? value-- : value = 0;
      }
      input.val(value + ' шт');
  });

	customInput.on('focus', function () {
    var validInputValue = parseInt($(this).val());
		 $(this).val('');
		 $(this).on('focusout', function () {
		 	var newValue = parseInt($(this).val());
			 if (newValue) {
			 		validInputValue = newValue
			 }
			 $(this).val(validInputValue + ' шт');
     })
  });

	var searchStars = $('.search__star-container');
	searchStars.each(function () {
		var percent = $(this).data('percent');
    var stars = $(this).find('.search__star');
    var percentStars = percent / 100 * stars.length;
		stars.each(function (i) {
			if (i < Math.floor(percentStars)){
				$(this).addClass('search__star--active')
			} else {
				if (i === Math.round(percentStars) - 1){
					$(this).addClass('search__star--half')
				}
			}
    });
  });

  $('.card__slider-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.card__slider-nav'
  });
  $('.card__slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.card__slider-content',
    dots: false,
		arrows: true,
    focusOnSelect: true,
		vertical: true,
		centerMode: true,
		swipe: false,
    centerPadding: 0
  });
  $('.card__simples-slide').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
		arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
        }
      }
		]
	})
});



